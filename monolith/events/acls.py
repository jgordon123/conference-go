from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_picture_url(query):
    """
    Get the URL of a picture from the Pexels API.
    """
    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]
    # can also do:
    # to do this method we have to 'import json' at the top of the page
    # print(json.loads(response.content))


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    descrip = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=descrip)
    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    descrip = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=descrip)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }


# Anna's version: Response returns it as a dictionary
# def get_picture_url(query):
#     """
#     Get the URL of a picture from the Pexels API
#     """
#     # Create a dictionary for the headers to use in the request
#     headers = {
#         "Authorization": f"{PEXELS_API_KEY}",
#     }
#     # Create the URL for the request with the city and state (aka query)
#     url = f"https://api.pexels.com/v1/search?query={query}"
#     # Make the request
#     response = requests.get(url, headers=headers)
#     # Parse the JSON response
#     api_dict = response.json()
#     # Return a dictionary that contains a 'picture_url' key and
#     # one of the URLs for one of the pictures in the response.
#     return {'picture_url': api_dict['photos'][0]['src']['original']}


# # Anna's code
# def get_weather_data(city, state):
#     """
#     Create the URL for the Geocoding API with the city and state
#     """
#     url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
#     # Make the request
#     response = requests.get(url)
#     # Parse the JSON response
#     data = response.json()[0]
#     # Get the latitude and longitude from the response
#     lat = data["lat"]
#     lon = data["lon"]
#     # Create the URL for the Weather API with the latitude and longitude
#     weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
#     # Make the request
#     response = requests.get(weather_url)
#     # Parse the JSON response
#     weather_data = response.json()
#     # Get the main temperature and the weather's description
#     temp = weather_data["main"]["temp"]
#     description = weather_data["weather"][0]["description"]
#     # and put them in a dictionary
#     weather = {
#         "temperature": temp,
#         "description": description
#     }
#     # Return the dictionary
#     return weather
